# Sucupira

Sucupira é um projeto de análise de dados para analizar a distribuição da árvore Sucupira, ou Pterodon emarginatus, no Brasil.

É um projeto curto cujo objetivo foi ajudar uma amiga, Thereza Marinho Lopes de Oliveira, engenheira florestal em seu Trabalho de Conclusão de Curso (TCC) na Universidade Federal do Rio Grande do Norte.

O projeto constituiu na análise dados geográficos e descriptivos da plata no Brasil, a organização e limpeza dos dados segundo as necessidades de Thereza e a exportação das bases de dados sobre a árvore Sucupira.

No final, foram gerados 10 arquivos diferentes (9 conjuntos de dados e 1 relatório).
